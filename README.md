# Addressing LHC challenges by ML

This repository contains materials and programming assignments for online course [Addressing Large Hadron Collider challenges by ML](https://www.coursera.org/learn/hadron-collider-machine-learning/home/welcome)

## Week 1

Introduction to particle physics for Data Scientists

[Programming Assignment: Z-boson mass measurement](week4/index.ipynb)


## Week 2

Particle identification
Detector composition
Particle identification with machine learning
[Programming Assignment: Particle identification](week2/Particle_identification.ipynb)

## Week 3

Search for New Physics in Rare Decays

[Programming Assignment](week3/index.ipynb)


## Week 4
Search for Dark Matter Hints with Machine Learning at new CERN experiment
[Search for electromagnetic showers](https://gitlab.com/mymooc/hadron-collider-machine-learning/blob/master/week4/index_xgb.ipynb)


## Week 5
Detector optimization
[Programming assignment](week5/tracker.ipynb)
